__author__ = 'Mathieu'

import httplib
import json


def test_search_controller(query):
    conn = httplib.HTTPConnection('localhost:5000')
    headers = {'Content-Type': 'application/json'}
    conn.request('POST', '/search', json.dumps(query), headers)
    rsp = conn.getresponse()
    content = rsp.read()
    return content

if __name__ == '__main__':
    print test_search_controller({'name': 'Dragonclaw'})