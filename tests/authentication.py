__author__ = 'Mathieu'

import httplib
import json
import time


def create_session():
    conn = httplib.HTTPConnection('localhost:5000')
    content = {"nickname": 'Mad2k6', 'password': 'test'}
    headers = {'Content-Type': 'application/json'}
    conn.request('POST', '/authentication', json.dumps(content), headers)
    rsp = conn.getresponse()
    content = rsp.read()
    return content


def refresh(content):
    conn = httplib.HTTPConnection('localhost:5000')
    headers = {'Content-Type': 'application/json'}
    conn.request('POST', '/authentication/refresh', content, headers)
    rsp = conn.getresponse()
    content = rsp.read()
    print content
    return content


def logout(content):
    conn = httplib.HTTPConnection('localhost:5000')
    headers = {'Content-Type': 'application/json'}
    conn.request('DELETE', '/authentication/logout', content, headers)
    rsp = conn.getresponse()
    rsp.read()


if __name__ == '__main__':
    content = create_session()
    for number_of_tests in range(1, 3):
        print "sleeping for 3 seconds: " + str(number_of_tests)
        time.sleep(3)
        # logout(content) # uncomment to remove session and trigger a 401
        content = refresh(content)

    logout(content)