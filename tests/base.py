__author__ = 'Mathieu'

import httplib


def test_index():
    conn = httplib.HTTPConnection('localhost:5000')
    headers = {'Content-Type': 'application/json'}
    conn.request('GET', '/', headers=headers)
    rsp = conn.getresponse()
    content = rsp.read()
    return content


def test_get_monster(id):
    conn = httplib.HTTPConnection('localhost:5000')
    headers = {'Content-Type': 'application/json'}
    conn.request('GET', '/monster/' + id, headers=headers)
    rsp = conn.getresponse()
    return rsp.read()


if __name__ == '__main__':
    print test_index()
    print test_get_monster('e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855')