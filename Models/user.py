__author__ = 'Mathieu Dugas'

from mongoengine import *
from werkzeug.security import check_password_hash

class User(Document):
    nickname = StringField(max_length=32, required=True)
    email = EmailField(required=True)
    password = StringField(required=True)
    permissions = MapField(field=BooleanField(),
                           default={'admin': False,
                                    'review': False,
                                    'moderator': False,
                                    'power_user': False,
                                    'anonymous': False,
                                    'banned': False
                                    },
                           required=True
                           )
    number_of_monster_submissions = IntField(default=0)
    number_of_art_submissions = IntField(default=0)
    active_monster_submissions = MapField(StringField())
    active_artwork_submission = MapField(StringField())
    last_known_ip = StringField()

    def authenticate(self, password):
        return check_password_hash(self.password, password)