__author__ = 'Mathieu Dugas'

from mongoengine import *

from user import User


class Monster(DynamicDocument):
    id = StringField(primary_key=True, unique=True)
    sources = ListField(StringField())
    author = ReferenceField(User)
    name = StringField()
    size = StringField()
    type = StringField()
    tags = ListField(StringField())
    alignment = StringField()
    armor_class = IntField()
    armor_class_type = StringField()
    hit_points = IntField()
    alternative_hit_points = MapField(IntField())
    speed = MapField(IntField())
    artwork = ObjectIdField()

    # This allows us to create the Submission class which will separate the submissions from the live data in the DB.
    meta = {'allow_inheritance': True}


class MonsterSubmission(Monster):
    date = DateTimeField()
    down_votes = IntField(default=0)
    up_vote = IntField(default=0)


class Artwork(Document):
    id = StringField(primary_key=True, unique=True)
    author = ReferenceField(User)
    reference_monster = ReferenceField(Monster)
    #artwork = ImageField()


    # This allows us to create the Submission class which will separate the submissions from the live data in the DB.
    meta = {'allow_inheritance': True}


class ArtworkSubmission(Artwork):
    date = DateTimeField()
    down_votes = IntField(default=0)
    up_vote = IntField(default=0)
