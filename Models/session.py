__author__ = 'Mathieu'

from mongoengine import *

import datetime

class Session(Document):
    user = ReferenceField('User')
    timestamp = DateTimeField(default=datetime.datetime.now)