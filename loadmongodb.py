__author__ = 'Mathieu Dugas'

import json
import hashlib
import os

from mongoengine import connect
from Models.monster import Monster
from config import *


connect(DevelopmentConfig.DATABASE,
        host=DevelopmentConfig.DATABASE_HOST,
        port=DevelopmentConfig.DATABASE_PORT
        )


def loadsinglemonster(monster_file):
    with open(monster_file) as opened_monster_file:
        raw_monster = json.load(opened_monster_file)

        monster = Monster(
            id=hashlib.sha256(opened_monster_file.read()).hexdigest(),
            alphabetical=raw_monster['name'][0],
            sources=raw_monster['sources'],
            author=raw_monster['author'],
            name=raw_monster['name'],
            size=raw_monster['size'],
            type=raw_monster['type'],
            tags=raw_monster['tags'],
            alignment=raw_monster['alignment'],
            armor_class=raw_monster['armor_class'],
            armor_class_type=raw_monster['armor_class_type'],
            hit_points=raw_monster['hit_points'],
            alternative_hit_points=raw_monster['alternative_hit_points'],
            speed=raw_monster['speed'],
            stats=raw_monster['stats'],
            properties=raw_monster['properties'],
            abilities=raw_monster['abilities'],
            actions=raw_monster['actions'],
        )

        if 'legendary_action_description' in raw_monster.keys():
            monster.legendary_action_description = raw_monster['legendary_action_description']
            monster.legendary_actions = raw_monster['legendary_actions']

        monster.save()


def loadrepository(base_folder):
    for source in os.listdir(base_folder):
        for monster in os.listdir(base_folder + '/' + source):
            loadsinglemonster(base_folder + '/' + source + '/' + monster)


if __name__ == '__main__':
    loadrepository('resources/Monsters')
