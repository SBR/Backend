__author__ = 'Mathieu Dugas'

import logging


class Config(object):
    #
    DEBUG = False
    DATABASE_HOST = 'localhost'
    DATABASE = 'sbr'
    LOGLEVEL = logging.INFO
    SECRET_KEY = 'Generate Secret Key Before Using'
    RECAPTCHA_PUBLIC_KEY = ''
    RECAPTCHA_PRIVATE_KEY = ''


class ProductionConfig(Config):
    # Uncomment the following line to set a mongodb server different than localhost
    # DATABASE_HOST = 'ip_of_live_mongodb'
    # Uncomment one of the following line to override the logging level
    # LOGLEVEL = logging.DEBUG
    # LOGLEVEL = logging.WARNING
    # LOGLEVEL = logging.CRITICAL
    pass

class DevelopmentConfig(Config):
    DEBUG = True
    LOGLEVEL = logging.DEBUG
