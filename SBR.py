__author__ = 'Mathieu Dugas'

from config import ProductionConfig, DevelopmentConfig
from flask import Flask
from mongoengine import connect
from logging import *
from logging.handlers import SMTPHandler

from Controllers.base import base_blueprint
from Controllers.monsters import monsters_blueprint
from Controllers.authentication import authentication_blueprint
from Controllers.user import user_blueprint
from Controllers.search import search_blueprint


# Generate a Flask application, configure it and import the blueprints
app = Flask(__name__)
app.config.from_object(DevelopmentConfig)
app.register_blueprint(base_blueprint)
app.register_blueprint(monsters_blueprint)
app.register_blueprint(authentication_blueprint)
app.register_blueprint(user_blueprint)
app.register_blueprint(search_blueprint)

# Connect to the mongodb database
connect(app.config['DATABASE'],
        host=app.config['DATABASE_HOST'],
        port=app.config['DATABASE_PORT']
        )

# Setup the Logger
basicConfig()
log = app.logger
log.setLevel(app.config['LOGLEVEL'])
if not app.debug:
    mail_handler = SMTPHandler(('mail.nabb.ca', 587), 'sbr@nabb.ca', app.config['LOGLEVEL'],
                               'SBR Has encountered an issue', app.config['EMAIL_CREDS'], ())
    mail_handler.setLevel(ERROR)
    app.logger.addHandler(mail_handler)

# Default start
if __name__ == '__main__':
    app.run()
