# Stat Block Repository Project

## Goal  
I started this project since I couldn't find any website that would allow the 
community to access every monsters from the official books of Wizards of the 
Coast and build a community around the site that allows user to share their own
idea of a good monster / memorable character. 

## License
I decided to go with the Apache 2.0 licence for this project as I want people to
be able to benefit from the source code of the project.

## Setup
1) Install the python development package for your distribution (on ubuntu it 
is called python-dev, on RHEL based distros it is called python-devel)  
2) Install the requirements inside your virtual environment (pip install -r 
REQUIREMENTS)  
3) Install MongoDB on your machine or on a server  
4) If the flask application runs on a different machine, install apache or nginx
and set it as a reverse proxy to port 5000 or use mod_wsgi to setup the
application

## Nota Bene
For the time being, we are two programmers on this project, but I do welcome others to submit ideas by opening
a ticket and I also welcome merge requests. Feature requests for this project are to be related to handling the backend
for the SBR project. Such feature could be an advanced search function. If you contribute to the project, don't forget
to add yourself to the CONTRIBUTORS file.
