__author__ = 'Mathieu Dugas'


from flask import Blueprint, abort, current_app, request, jsonify
from Models.session import Session
from Models.monster import Monster
from mongoengine import DoesNotExist
import authentication

monsters_blueprint = Blueprint('monsters', __name__, template_folder='templates', url_prefix='/monsters')


def get_filter_types():
    filter_types = {
        'alphabetical': Monster.objects.distinct('alphabetical'),
        'challenge': Monster.objects.distinct('properties.Challenge'),
        'source': Monster.objects.distinct('sources'),
        'size': Monster.objects.distinct('size'),
        'type': Monster.objects.distinct('type'),
        'tags': Monster.objects.distinct('tags'),
        'alignment': Monster.objects.distinct('alignment')
    }
    return filter_types


@monsters_blueprint.route('/', methods=['GET'])
def get_all_monsters():
    return get_all_monsters_by_page(1)


@monsters_blueprint.route('/page/<page>', methods=['GET'])
def get_all_monsters_by_page(page):
    try:
        # Grab 10 monsters based on the page.
        monster_objects = Monster.objects().order_by('name')[int(page)-1:11]

        # Convert the objects to JSON.
        monsters_json_format = []
        for monster in monster_objects:
            monsters_json_format.append(monster.to_json())

        # Returns the 10 monsters.
        return jsonify(monsters=monsters_json_format)

    except ValueError:
        # If the page isn't an integer, return a bad request error.
        current_app.logger.error("Someone is trying to screw around with the pages! Returning an error 400")
        abort(400)


@monsters_blueprint.route('/by/<filter_type>')
def get_monster_filter_term(filter_type):
    # If the filter type is valid, return it's valid terms.
    if filter_type in get_filter_types():
        return jsonify(subtypes=get_filter_types().get(filter_type))
    # Otherwise return that this filter type hasn't been implemented yet.
    abort(501)


@monsters_blueprint.route('/by/<filter_type>/<filter_term>')
def get_monster_by_type_and_term(filter_type, filter_term):
    return get_monster_by_type_and_term_by_page(filter_type, filter_term, 1)


@monsters_blueprint.route('/by/<filter_type>/<filter_term>/page/<page>')
def get_monster_by_type_and_term_by_page(filter_type, filter_term, page):
    try:
        # Check if filter_type is valid.
        if filter_type in get_filter_types():
            # Check if filter_term is valid.
            if filter_term in get_filter_types().get(filter_type):
                # Generate a json array of 10 monsters fitting this filter type/term.
                monster_objects = Monster.objects(__raw__={filter_type: filter_term})[int(page)-1:11]
                monster_array = []
                for monster in monster_objects:
                    monster_array.append(monster.to_json())

                # Return the array
                return jsonify(monsters=monster_array)
        # Return a not implemented if the filter_type or filter_term is not valid.
        abort(501)

    except ValueError:
        # If the page isn't an integer, then we are dealing with a simple bad request.
        current_app.logger.error("Someone is trying to screw around with the pages! Returning an error 400")
        abort(400)


@monsters_blueprint.route('/by/id/<monster_id>', methods=['GET', 'PUT', 'DELETE'])
def get_monster_by_id(monster_id):
    # If it is a GET request, process it immediately.
    if request.method == 'GET':
        try:
            # Return the monster if it exists.
            monster = Monster.objects.get(id=monster_id)
            current_app.logger.debug('get_monster: ' + monster.id)

            return monster.to_json()
        except DoesNotExist:
            # Return a 404 if it doesn't exist.
            abort(404)

    # Since it is not a GET request, let's grab the session ID.
    session_id = authentication.get_sessionID_from_request(request)
    if session_id != None:
        try:
            # Since we validated the provided token, now we can check the user's session.
            session = Session.objects.get(id=session_id)

            # Insert/Update the provided monster if it is a PUT request and the authenticated user is an admin.
            if request.method == 'PUT' and session.user.permissions['admin']:
                monster_json = request.get_json().get('monster')
                try:
                    # Update
                    monster = Monster.objects.get(id=monster_id)
                    monster.delete()
                    monster = Monster.from_json(monster_json)
                    monster.save()
                    return jsonify(id=monster.id), 200

                except DoesNotExist:
                    # Insert
                    monster = Monster.from_json(monster_json)
                    monster.save()
                    return jsonify(id=monster.id), 201

            # Delete the monster with the ID provided if it is a DELETE request and the authenticated user is an admin.
            elif request.method == 'DELETE' and session.user.permissions['admin']:
                try:
                    monster = Monster.objects.get(id=monster_id)
                    monster.delete()
                except DoesNotExist:
                    # Returns a 404 if the monster already doesn't exists in the DB.
                    abort(404)

            if not session.user.permissions['admin']:
                abort(401)

            abort(501)
        except DoesNotExist:
            # Invalid session token, user has to re-authenticate.
            abort(302)
    else:
        abort(302)
