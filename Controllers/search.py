__author__ = 'Mathieu Dugas'

from flask import Blueprint, request, jsonify, current_app

import json

from Models.monster import Monster

search_blueprint = Blueprint('search', __name__,)


@search_blueprint.route('/search', methods=['POST'])
def search():
    current_app.logger.debug('search: Entered search function')
    query = json.dumps(request.get_json())
    current_app.logger.debug('search: ' + query)
    if query.count(':') > 0 or (query.count('"') == 2 and query[0] != '"' and query[-1] != '"'):
        current_app.logger.debug('search: Field based search')
        # search_terms_dictionary = get_search_terms(query)
        search_terms_dictionary = query
    else:
        current_app.logger.debug('search: Single item search')
        # Strip some characters from the query as we know that is is a single word.
        single_search_term = query.strip().strip('"').strip('\'').strip('{').strip('}')
        current_app.logger.debug('search: Stripped the search term from quotes and curly brackets')

        # Exclude the Public source and the Monster Manual as we don't want to return an extremely huge result set.
        if single_search_term.lower() == 'public' or single_search_term.lower == 'monster manual':
            current_app.logger.debug('search: the source is public or monster manual within the query')
            possible_source = 'none'
        else:
            current_app.logger.debug('search: the source is not on the exclusion list')
            possible_source = single_search_term

        # Construct the raw query which is a basic OR on our displayed lists. using regex for the name and the type
        search_terms_dictionary = {
                                    '$or': [
                                        {'name': {'$regex': '.*' + single_search_term + '.*', '$options': 'i'}},
                                        {'properties.Challenge': single_search_term},
                                        {'size': {'$regex': '^' + single_search_term + '$', '$options': 'i'}},
                                        {'sources': {'$regex': '^' + possible_source + '$', '$options': 'i'}},
                                        {'type': {'$regex': '^' + single_search_term + '$', '$options': 'i'}},
                                        {'tags': {'$regex': '.*' + single_search_term + '.*', '$options': 'i'}},
                                    ]
                                   }
    current_app.logger.debug('search: ' + search_terms_dictionary.__str__())
    query_set = Monster.objects(__raw__={"name": "Dragonclaw"})
    current_app.logger.debug('search: We have monsters!')
    monsters = {}
    for monster in query_set:
        current_app.logger.debug('search: Monster: ' + monster.id)
        monsters[monster.id] = monster.name
    current_app.logger.debug('search: MonsterS: ' + monsters.__str__())
    return jsonify(monsters)


def get_search_terms(query):
    search_terms_string = query.split(',')  # Separate the query into individual search terms.
    current_app.logger.debug('get_search_terms: Split the query in individual terms')
    search_terms_dictionary = {}
    current_app.logger.debug('get_search_terms: For each term')
    for string in search_terms_string:  # Add each search term to the dictionary
        field = string.split(':')[0].strip()
        value = string.split(':', 1)[1].strip()  # Grab the value and strip spaces
        current_app.logger.debug('get_search_terms:    Split the term from the field')
        if value.startswith('{') and value.count(':') > 0:  # If it is a sub query ie: a greater or equal than
            current_app.logger.debug('get_search_terms:       Value contains curly braces. This means it\'s a sub query')
            value = value.strip('{').strip('}')  # Strip the curly braces and the spaces of the substring.
            current_app.logger.debug('get_search_terms:       Stripped the curly braces.')
            current_app.logger.debug('get_search_terms:       recursive loop on the sub query')
            value = get_search_terms(value)

        # Check if the value is a number, except for challenge rating
        elif unicode(value).isnumeric() and field != 'properties.Challenge':
            current_app.logger.debug("get_search_terms: Value is an integer")
            value = int(value)
        elif value.find('.') != -1:  # Otherwise handle it like a float
            try:
                current_app.logger.debug("get_search_terms: Value may be a float")
                value = float(value)
            except ValueError:
                current_app.logger.error("get_search_terms: Value is not a number")
        search_terms_dictionary[field] = value
        current_app.logger.debug("get_search_terms: Add terms to dict")
    return search_terms_dictionary
