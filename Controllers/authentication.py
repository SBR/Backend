__author__ = 'Mathieu Dugas'

from flask import Blueprint, request, redirect, abort, jsonify, current_app
from Models.session import Session
from mongoengine import DoesNotExist

import jwt

from Models.user import User

authentication_blueprint = Blueprint('authenticate', __name__,)

@authentication_blueprint.route('/authentication', methods=['POST'])
def login():
    credentials = request.get_json()
    current_app.logger.debug("login: Credentials received")
    try:
        user = User.objects.get(nickname=credentials['nickname'])
        current_app.logger.debug("login: User found")
        if user.authenticate(credentials['password']):
            current_app.logger.debug("login: Password confirmed")
            return jsonify(token=get_session_token(Session(user=user).save()))
        else:
            current_app.logger.debug("Password invalid")
            return abort(401)
    except DoesNotExist:
        current_app.logger.debug("User invalid")
        return abort(401)


@authentication_blueprint.route('/authentication/refresh', methods=['POST'])
def refresh_session():
    try:
        session = Session.objects.get(id=get_sessionID_from_request(request))
        current_app.logger.debug("refresh_session: Current session retreived")
        user = session.user
        current_app.logger.debug("refresh_session: User retrieved from previous session")
        session.delete()
        current_app.logger.debug("refresh_session: Removed old session object")
        return jsonify(token=get_session_token(Session(user=user).save()))

    except DoesNotExist:
        current_app.logger.debug("refresh_session: Session already expired")
        return abort(401)


@authentication_blueprint.route('/authentication/register', methods=['GET', 'POST'])
def register():
    abort(501)


@authentication_blueprint.route('/authentication/logout', methods=['DELETE'])
def logout():
    try:
        current_app.logger.debug('logout: Entry')
        session = Session.objects.get(id=get_sessionID_from_request(request))
        current_app.logger.debug('logout: Session retrived')
        session.delete()
        current_app.logger.debug('logout: Session deleted')
        return redirect('/')

    except DoesNotExist:
        return redirect('/')

def get_sessionID_from_request(request):
    try:
        jwt_session_token = request.get_json()
        current_app.logger.debug("get_sessionID_from_request: JSON from request parsed")
        sbr_session_token = jwt.decode(jwt_session_token['token'], current_app.config['SECRET_KEY'])
        current_app.logger.debug("get_sessionID_from_request: Session token decoded")
        return sbr_session_token['sessionID']
    except jwt.DecodeError:
        # Someone was trying to bypass the system. Log their IP as reference and possible ban offence.
        current_app.logger.error("Someone is trying to hack the system. IP: " + request.remote_addr)
        return None

def get_session_token(session):
    payload = {'sessionID': str(session.id)}
    current_app.logger.debug('get_session_token: payload created')
    return jwt.encode(payload, current_app.config['SECRET_KEY'])