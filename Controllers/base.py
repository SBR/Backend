__author__ = 'Mathieu Dugas'

from flask import Blueprint, jsonify, current_app
from Models.monster import Monster, MonsterSubmission

base_blueprint = Blueprint('base', __name__, template_folder='templates')



@base_blueprint.route('/', methods=['GET'])
def index():
    return jsonify(version=current_app.config['VERSION'],
                   total_monsters=Monster.objects.count(),
                   total_submissions=MonsterSubmission.objects.count()
                   )
